import os
import random
from discord.ext import commands
from dotenv import load_dotenv

load_dotenv() #cerca un file nella directory chiamato .env

TOKEN = os.getenv("DISCORD_TOKEN")
GUILD = os.getenv("DISCORD_GUILD")

bot = commands.Bot( command_prefix="!") #l'utente per lanciare il metodo del bot dovrà scrivere <!comando>

@bot.command(name = "quote", help = "Returns a quote from various video games")
async def get_quote(context):
    quotes_list = [
        "It’s time to kick ass and chew bubble gum…and I’m all outta gum. - Duke Nukem",
        "They were all dead. The final gunshot was an exclamation mark to everything that had led to this point. I released my finger from the trigger. And then it was over. - Max Payne",
        "You were almost a Jill sandwich! - Resident Evil",
        "I'm Ezio Auditore da Firenze - Assassin's Creed 2",
        "It’s dangerous to go alone, take this! - The Legend of Zelda",
        "Praise the sun! - Dark Souls",
        "I used to be an adventurer like you until I took an arrow to the knee. - The Elder Scrolls V: Skyrim",
        "What is bravery, without a dash of recklessness? - Dark Souls",
        "Good! Now we can fight as warriors. Hand-to-hand, it is the basis of all combat. Only a fool trusts his life to a weapon. - Metal Gear Solid",
        "Did I ever tell you the definition of insanity? - Far Cry 3",
        "No Russian. - Call of Duty: Modern Warfare 2",
    ]

    response = random.choice(quotes_list)
    await context.send(response)


bot.run(TOKEN)

